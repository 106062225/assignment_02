// Initialize Phaser
var game = new Phaser.Game(480, 600, Phaser.AUTO, 'canvas'); 
// Define our global variable
game.global = { score: 0, level: 1, life: 5, timeleft:10, enemydelay:2000};
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('teach', teachState);
// Start the 'boot' state
game.state.start('boot');