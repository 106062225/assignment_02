var playState = { 
    preload: function() {
    }, 
    create: function() {
        //game.stage.backgroundColor = '#3498db';
        this.sea = game.add.tileSprite(0, 0, 800, 600, 'sea');
        //var gameLabel = game.add.text(game.width/2, 150,'game', { font: '30px Arial', fill: '#ffffff' });
        //gameLabel.anchor.setTo(0.5, 0.5);
        //player
        //this.player = game.add.sprite(game.width/2, game.height/2+200, 'player');
        //game.physics.arcade.enable(this.player);
        //this.player.anchor.setTo(0.5, 0.5);
        //playernew
        this.playernew = game.add.sprite(game.width/2, 700, 'playernew');
        game.physics.arcade.enable(this.playernew);
        this.playernew.anchor.setTo(0.5, 0.5);
        //this.playernew.body.gravity.y = 500;
        this.playernew.animations.add('fly',[0, 1, 2, 3, 4], 10, true);
        this.playernew.speed = 300;
        this.playernew.body.collideWorldBounds = true;
        //keyboard
        this.cursor = game.input.keyboard.createCursorKeys();
        //add enemy
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(10, 'enemynew');
        this.enemies.setAll('anchor.x, 0.5');
        this.enemies.setAll('anchor.y, 0.5');
        this.enemies.setAll('outOfBoundsKill', true);
        this.enemies.setAll('checkWorldBounds', true);
        //this.enemies.setAll('body.collideWorldBounds', true);
        this.enemies.forEach(function(enemy) {enemy.animations.add('fly',[0, 1, 2], 20, true);});
        this.nextEnemyAt = 0;
        this.enemyDelay = game.global.enemydelay;
        game.time.events.loop(1000, this.enemymove, this);
        //item
        this.items = game.add.group();
        this.items.enableBody = true;
        this.items.createMultiple(10, 'item');
        this.items.setAll('anchor.x, 0.5');
        this.items.setAll('anchor.y, 0.5');
        this.items.setAll('outOfBoundsKill', true);
        this.items.setAll('checkWorldBounds', true);
        //enemybullet
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.createMultiple(100, 'bullet');
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);
        //bullet
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(100, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 0.5);
        this.nextShotAt = 0;
        this.shotDelay = 100;
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
        this.bigbullets = game.add.group();
        this.bigbullets.enableBody = true;
        this.bigbullets.createMultiple(100, 'bigbullet');
        this.bigbullets.setAll('anchor.x', 0.5);
        this.bigbullets.setAll('anchor.y', 0.5);
        this.bigbullets.setAll('outOfBoundsKill', true);
        this.bigbullets.setAll('checkWorldBounds', true);
        //game.time.events.add(5000, this.levelup, this);
        //timer
        this.timeleft = 10;
        this.timelable = game.add.text(game.width/2, 150, '敵軍過境剩餘時間：' + game.global.timeleft,  { font: '30px Arial', fill: '#000000' });
        this.timelable.anchor.setTo(0.5, 0.5);
        //this.levellable = game.add.text(game.width/2, 150, game.global.level, { font: '30px Arial', fill: '#ffffff' });
        game.time.events.loop(Phaser.Timer.SECOND, this.timer, this);
        //sound
        this.explosionsound = game.add.audio('explosionsound');
        this.fireSound = game.add.audio('firesound');
        this.powerupSound = game.add.audio('powerupsound');
        //score
        this.scoreLabel = game.add.text(game.width/2, game.height/2,'score: ' + game.global.score, { font: '50px Arial', fill: '#ffffff' });
        this.scoreLabel.anchor.setTo(0.5, 0.5);
        //life
        //this.lifeLabel = game.add.text(game.width/2, game.height/2+50, game.global.life, { font: '50px Arial', fill: '#ffffff' });
        //this.lifeLabel.anchor.setTo(0.5, 0.5);
        //pixel
        this.emitter = game.add.emitter(0, 0, 15);
        this.emitter.makeParticles('burst');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 0;
        //fevertime
        this.fevertime = false;
        this.protecttime = false;
        this.shield = game.add.sprite(game.width/2, 520, 'shield');
        game.physics.arcade.enable(this.shield);
        this.shield.anchor.setTo(0.5, 0.5);
        this.shield.body.collideWorldBounds = true;
        this.shield.kill();
        /*game.time.events.add(2000, function(){
            this.shield.reset(),
            this.shield.x = this.playernew.x;
            this.shield.y = this.playernew.y-30;
        }, this);*/
        //skill
        this.skillready = true;
        this.skillneed = false;
        this.skillicon = game.add.sprite(400, 560 ,'skillicon');
        //heart
        this.addheart();
    },
    update: function() {
        this.sea.tilePosition.y += 2;
        game.physics.arcade.overlap(this.playernew, this.items, this.choose, null, this);
        game.physics.arcade.overlap(this.bullets, this.enemies, this.enemyHit, null, this);
        game.physics.arcade.overlap(this.bigbullets, this.enemies, this.enemyHit, null, this);
        game.physics.arcade.overlap(this.playernew, this.enemies, this.playerHit, null, this);
        game.physics.arcade.overlap(this.playernew, this.enemyBullets, this.playerHit, null, this);
        if (this.nextEnemyAt<game.time.now && this.enemies.countDead()>0) {
            this.nextEnemyAt = game.time.now + this.enemyDelay;
            var enemy = this.enemies.getFirstExists(false);
            enemy.reset(game.rnd.integerInRange(20, 460), 0);
            enemy.anchor.setTo(0.5, 0.5);
            enemy.body.velocity.y = game.rnd.integerInRange(40, 80);
            enemy.body.velocity.x = 200;
            enemy.play('fly');
        }
        if(this.skillneed){
            this.skillsetup();
            this.skillneed = false;
        };
        this.enemyfire();
        this.moveplayer();
    },
    moveplayer: function(){
        if(this.cursor.left.isDown){
            //this.player.body.velocity.x = -200;
            this.playernew.body.velocity.x = -200;
            this.playernew.animations.play('fly');
            this.shield.body.velocity.x = -200;
            //this.player.animations.play('leftwalk');
        }
        else if(this.cursor.right.isDown){
            //this.player.body.velocity.x = 200;
            this.playernew.body.velocity.x = 200;
            this.playernew.animations.play('fly');
            this.shield.body.velocity.x = 200;
            //this.player.animations.play('rightwalk');
        }
        else if(this.cursor.up.isDown){
            //this.player.body.velocity.y = -200;
            this.playernew.body.velocity.y = -200;
            this.playernew.animations.play('fly');
            this.shield.body.velocity.y = -200;
        }
        else if(this.cursor.down.isDown){
            //this.player.body.velocity.y = 200;
            this.playernew.body.velocity.y = 200;
            this.playernew.animations.play('fly');
            this.shield.body.velocity.y = 200;
        }
        else{
            //this.player.body.velocity.x = 0;
            //this.player.body.velocity.y = 0;
            this.playernew.body.velocity.x = 0;
            this.playernew.body.velocity.y = 0;
            this.shield.body.velocity.x = 0;
            this.shield.body.velocity.y = 0;
            //this.player.animations.stop();
            //this.player.frame = 0;
        }
        if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            this.fire();
        }
        if(game.input.keyboard.isDown(Phaser.Keyboard.Z) && this.skillready){
            this.skillready = false;
            this.skillneed = true;
            this.skillicon.kill();
            this.skill();
        }
        if(game.input.keyboard.isDown(Phaser.Keyboard.M)){
            if(this.explosionsound.volume){
                this.explosionsound.volume = 0;
                this.fireSound.volume = 0;
            }
            else{
                this.explosionsound.volume = 1;
                this.fireSound.volume = 1;
            }
        }
    },
    enemyHit: function(bullet, enemy) {
        game.global.score += 10;
        this.scoreLabel.text = 'score: ' + game.global.score;
        this.explosionsound.play();
        bullet.kill();
        enemy.kill();
        var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
        var item = this.items.getFirstExists(false);
        item.reset(enemy.x, enemy.y);
        item.anchor.setTo(0.5, 0.5);
        item.body.velocity.y = 300;
    },
    playerHit: function(player, enemy) { 
        if(this.protecttime){return;}
        this.emitter.x = this.playernew.x;
        this.emitter.y = this.playernew.y;
        this.emitter.start(true, 800, null, 15);
        this.explosionsound.play();
        game.global.life--;
        this.heartbye();
        //this.lifeLabel.text = game.global.life;
        if(game.global.life>0){
            enemy.kill();
            game.camera.flash(0xffffff, 300);
            game.camera.shake(0.02, 300);
            //game.add.tween(this.lifeLabel.scale).to({x:2, y:2}, 500).yoyo(true).start();
        }
        else{
            game.global.life = 5;
            this.explosionsound.play();
            enemy.kill();
            var explosion = game.add.sprite(player.x, player.y, 'explosionbig');
            explosion.anchor.setTo(0.5);
            explosion.animations.add('boom');
            explosion.play('boom', 15, false, true);
            player.kill();
            game.global.enemydelay = 2000;
            var overLabel = game.add.text(game.width/2, game.height/2, 'gameover', { font: '50px Arial', fill: '#ff0000' });
            overLabel.anchor.setTo(0.5, 0.5);
            game.add.tween(overLabel.scale).to({x:2, y:2}, 500).yoyo(true).start();
            game.time.events.add(1000, function(){
                game.global.level = 1;
                game.state.start('menu')
            }, this);
        }
    },
    fire: function() { 
        if(this.fevertime){
            if (!this.playernew.alive || this.nextShotAt>game.time.now) {
                    return;
                }
                if (this.bullets.countDead()==0) {
                    return;
                }
                this.fireSound.play();
                this.nextShotAt = game.time.now + this.shotDelay;
                var bullet = this.bigbullets.getFirstExists(false);
                bullet.reset(this.playernew.x, this.playernew.y-20);
                bullet.body.velocity.y = -500;
        }else{
            if (!this.playernew.alive || this.nextShotAt>game.time.now) {
                return;
            }
            if (this.bullets.countDead()==0) {
                return;
            }
            this.fireSound.play();
            this.nextShotAt = game.time.now + this.shotDelay;
            var bullet = this.bullets.getFirstExists(false);
            bullet.reset(this.playernew.x, this.playernew.y-20);
            bullet.body.velocity.y = -500;
        }
    },
    enemyfire: function(){
        this.enemies.forEachExists(function(enemy) {
			var bullet = this.enemyBullets.getFirstExists(false);
            if(bullet) {
                if(game.time.now > (enemy.bulletTime || 0)) {
                bullet.reset(enemy.x, enemy.y);
                bullet.body.velocity.y = 200;
                enemy.bulletTime = game.time.now + Math.random()*2000;
                }
            }
		}, this);
    },
    enemymove: function(){
        this.enemies.forEachExists(function(enemy) {
            enemy.body.velocity.x *= (-1)*(Math.random()+0.5);
		}, this);
    },
    skill: function(){
        game.camera.flash(0xffffff, 300);
        game.camera.shake(0.02, 300);
        this.enemies.forEachExists(function(enemy){
            enemy.kill();
            game.global.score += 10;
        }, this);
        this.scoreLabel.text = 'score: ' + game.global.score;
    },
    levelup: function(){
        game.stage.backgroundColor = '#3498db';
        var gameLabel = game.add.text(game.width/2, game.height/2,'levelup', { font: '50px Arial', fill: '#ff0000' });
        gameLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(gameLabel.scale).to({x:2, y:2}, 500).yoyo(true).start();
        game.global.level++;
        game.global.timeleft = game.global.level*10;
        game.global.enemydelay -=250;
        game.time.events.add(1000, function(){game.state.start('play')}, this);
        //this.levellable = game.add.text(game.width/2, 150, game.global.level, { font: '30px Arial', fill: '#ffffff' });
    },
    timer: function(){
        if(game.global.timeleft==1){this.levelup();}
        game.global.timeleft--;
        this.timelable.text = '敵軍過境剩餘時間：' + game.global.timeleft;
    },
    choose: function(player, item){
        this.powerupSound.play();
        var ran = Math.random();
        game.global.score += 5;
        this.scoreLabel.text = 'score: ' + game.global.score;
        if(ran<0.5){
            this.fever(player, item);
        }
        else{
            this.protect(player, item);
        }
    },
    fever: function(player, item){
        item.kill();
        this.fevertime = true;
        game.time.events.add(2000, function(){this.fevertime=false}, this);
    },
    protect: function(player, item){
        item.kill();
        this.protecttime = true;
        this.shield.reset(this.playernew.x, this.playernew.y-30),
        game.time.events.add(5000, function(){
            this.protecttime = false,
            this.shield.kill()
        }, this);
    },
    skillsetup: function(){
        game.time.events.add(6000, function(){
            this.skillready = true;
            this.skillicon.reset(400, 560);
        }, this);
    },
    heartbye: function(){
        this.hearts.getFirstAlive(true).kill();
    },
    addheart: function(){
        this.hearts = game.add.group();
        for(var i=1; i<=game.global.life; i++){
            game.add.sprite(200-i*30, 570, 'heart', 0, this.hearts);
        }
    }
};