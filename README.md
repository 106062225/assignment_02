# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 

1. Jucify mechanisms : 
if save after countdown, automatically enter next level. higher level get longer countdown, enemy appear faster, also they shoot faster.
skill being available every five second, press Z to use skill, kill all enemy on the screen, but not bullet lol.
2. Animations :
player's aircraft do have animation while flying, though it seems strange. when enemy is hit, they breakdown with animation, so does the player's aircraft.
3. Particle Systems :
when player's aircraft is hit by bullet or enemy itself, particle system works, imitating explosion.
4. Sound effects :
shooting sound when player shoot. explosion sound when either enemy or player's aircraft was hit. power up sound when successfully collect the item also, press M to turn on/off sound effect.
5. Leaderboard :
store score in localstorage lol. you can see best score and your current score.

# Bonus Functions Description : 
1. items :
enemy drop items after being hit, item make player's bullets bigger for few second or give player a shield to prevent form attack by enemy's bullet and colliding for few seconds. either of them depends on probability Math.random().
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
