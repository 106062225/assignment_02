var menuState = { 
    create: function() {
        game.stage.backgroundColor = '#ffffff';
        this.sea = game.add.tileSprite(0, 0, 800, 600, 'sea');
        var menuLabel = game.add.text(game.width/2, 150,'雷電HT-II', { font: '30px Arial', fill: '#111111' });
        menuLabel.anchor.setTo(0.5, 0.5);
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        upKey.onDown.add(this.start, this); 
        downKey.onDown.add(this.howtoplay, this);
        var helpLabel = game.add.text(game.width/2, 500,
            '↑ start to play\n↓ see how to play', 
            { font: '30px Arial', fill: '#111111' });
        helpLabel.anchor.setTo(0.5, 0.5);
        
        if (!localStorage.getItem('bestScore')) { 
            localStorage.setItem('bestScore', 0);
            // Set the best score to 0
        }
        // If the score is higher than the best score
        if (game.global.score > localStorage.getItem('bestScore')) {
            // Then update the best score
            localStorage.setItem('bestScore', game.global.score);
        }
        var scoreLabel = game.add.text(game.width/2, game.height/2, 'score: ' + game.global.score + '\nbest score: ' + localStorage.getItem('bestScore'), { font: '25px Arial', fill: '#111111' });
        scoreLabel.anchor.setTo(0.5, 0.5);
    },
    start: function() {
        // Start the actual game
        game.global.score = 0;
        game.state.start('play');
    }, 
    howtoplay: function(){
        game.state.start('teach');
    }
};