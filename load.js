var loadState = { 
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        //game.load.image('progressBar', 'assets/wallH.png');
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // Load all game assets
        game.load.image('player', 'assets/player.png');
        //game.load.spritesheet('player', 'player2.png', 20, 20);
        //game.load.image('wallH', 'wallH.png');
        //game.load.image('wallV', 'wallV.png');
        //game.load.image('coin', 'coin.png');
        game.load.image('enemy', 'assets/enemy.png');
        game.load.image('pixel', 'assets/pixel.png')
        // Load a new asset that we will use in the menu state
        //game.load.image('background', 'assets/background.png'); 
        game.load.image('sea', 'assets/background.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('bigbullet', 'assets/bullet-burst.png');
        game.load.spritesheet('enemynew', 'assets/enemynew.png', 32, 32);
        game.load.spritesheet('explosion', 'assets/explosion.png', 50, 50);
        game.load.spritesheet('explosionbig', 'assets/explosionbig.png', 70, 70);
        game.load.spritesheet('playernew', 'assets/playernew.png', 93, 100);
        //sound
        game.load.audio('explosionsound', ['assets/explosion.ogg', 'assets/explosion.wav']);
        game.load.audio('firesound', ['assets/player-fire.ogg', 'assets/player-fire.wav']);
        game.load.audio('powerupsound', ['assets/powerup.ogg', 'assets/powerup.wav']);
        //pixel
        game.load.image('burst', 'assets/bullet-burst.png');
        //item
        game.load.image('item', 'assets/coin.png');
        game.load.image('shield', 'assets/shield.png');
        //boss
        game.load.image('boss', 'assets/bossboss.png');
        //heart
        game.load.image('heart', 'assets/heart.png');
        //skill
        game.load.image('skillicon', 'assets/skill.png');
    },
    create: function() {
        // Go to the menu state
        //game.state.start('menu');
        game.time.events.add(500, function(){game.state.start('menu');}, this);
    } 
};