var teachState = { 
    create: function() {
        game.stage.backgroundColor = '#ffaaaf';
        //var teachLabel = game.add.text(game.width/2, 150,'teach', { font: '30px Arial', fill: '#111111' });
        //teachLabel.anchor.setTo(0.5, 0.5);
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this); 
        var teachLabel = game.add.text(game.width/2, 300,
            '↑↓←→ to control\nZ to use fatal blow\nSpace to shoot\nM to turn on/off sound\n', 
            { font: '30px Arial', fill: '#111111' });
        teachLabel.anchor.setTo(0.5, 0.5);
        var teachLabel2 = game.add.text(game.width/2, 500,
            'collect items drop from enemies\nto strength your aircraft', 
            { font: '20px Arial', fill: '#111111' });
        teachLabel2.anchor.setTo(0.5, 0.5);
        this.teachLabel3 = game.add.text(game.width/2, 550,
            'press ↑ get started', 
            { font: '30px Arial', fill: '#111111' });
        this.teachLabel3.anchor.setTo(0.5, 0.5);
        game.time.events.add(200, this.changewhite, this);
    },
    start: function() {
        // Start the actual game
        game.state.start('play');
    }, 
    changewhite: function(){
        this.teachLabel3.addColor('#ffffff', 0);
        game.time.events.add(100, this.changeblack, this);
    },
    changeblack: function(){
        this.teachLabel3.addColor('#111111', 0);
        game.time.events.add(100, this.changewhite, this);
    }
};